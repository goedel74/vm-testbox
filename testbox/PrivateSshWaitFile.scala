import java.net.InetSocketAddress
import java.nio.file._
import java.security.PublicKey
import scala.collection.JavaConverters._
import scala.util.control.NonFatal

import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory
import org.apache.sshd.server.SshServer
import org.apache.sshd.server.auth.password.PasswordAuthenticator
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider
import org.apache.sshd.server.scp.ScpCommandFactory
import org.apache.sshd.server.session.ServerSession


object PrivateSshWaitFile extends App {
	lazy val homeDir = Paths.get("testbox").resolve("runtime") // 固定
	/*
		[on server = host = localhost]
		cd .ssh
		ssh-keygen -t rsa ・・・ generates id_rsa and id_rsa.pub files
		mv id_rsa.pub authorized_keys ・・・ cat id_rsa.pub >> authorized_keys
		chmod 700 .ssh/
		chmod 600 .ssh/authorized_keys

		※ id_rsa を guest os (ssh クライアントマシン)に送信
		[on client = guest]
		chmod 700 .ssh/
		chmod 600 .ssh/id_rsa
		ssh -P 19022 sample.txt vm@10.0.2.2:path/to
		※ 初回 ssh 接続時(i.e. 初めて ssh コマンドで host に接続した時) .ssh/known_hosts ファイルが生成される
		※ id_rsa の名前は変更可能だが ssh コマンド発行の際に -i オプションで(変更後のファイル名を)指定
	*/
	lazy val authorizedKeys = homeDir.resolve("sshd").resolve("authorized_keys")

	def validSession(session: ServerSession) = {
		val remote = session.getClientAddress.asInstanceOf[InetSocketAddress].getHostName
		println(s"try to connect ... from: $remote")

		// RFC 5735 : Special Use IPv4 Addresses
		remote.startsWith("127.0.0.") || // 127.0.0.0/8 (Loopback) RFC 1122
		remote.startsWith("192.168.") || // 192.168/16 (Class A) RFC 1918
		remote.startsWith("172.16.") || // 172.16/12 (Class B) RFC 1918
		remote.startsWith("10.") || // 10/8 (Class C) RFC 1918
		// remote.startsWith("0.") || // RFC 5735
		// remote.startsWith("169.254.") || // 169.254.0.0/16 (Link Local) RFC 3927
		// remote.startsWith("224.") || // 224.0.0.0 to 239.255.255.255 (Multicast) RFC 1112
		// remote.startsWith("240.") || // 240.0.0.0/4 (Reserved)
		// remote.startsWith("255.255.255.255") || // RFC 0919 / RFC 0922
		remote == "localhost"
		// and more ... Don't forget: the private key has been committed
	}

	def pubkeyAuthenticator = new PublickeyAuthenticator() { // 公開鍵認証方式
		// If false, client could see 'Permission denied'
		override def authenticate(username: String, pk: PublicKey, session: ServerSession) = {
			validSession(session) && username == "vm"
		}
	}

	def passwordAuthenticator = new PasswordAuthenticator() { // パスワード認証方式
		override def authenticate(username: String, pwd: String, session: ServerSession) = {
			validSession(session) && username == "vm" && pwd == ""
		}
	}

	new PrivateSshWaitFile().run(
		fileToWait = Paths.get(args(0)).toAbsolutePath,
		portNumber = args(1).toInt
	)
}

class PrivateSshWaitFile(homeDir: Path = PrivateSshWaitFile.homeDir, authorizedKeys: Path = PrivateSshWaitFile.authorizedKeys) {
	import PrivateSshWaitFile.{passwordAuthenticator, pubkeyAuthenticator}

	// タイムアウト値は呼び出し元(外部)で制御してもらう想定
	/** ファイルが scp で送信されてくることを待機 */
	def run(fileToWait: Path, portNumber: Int): Unit = {
		val sshd = SshServer.setUpDefaultServer
		try {
			sshd.setPort(portNumber)
			sshd.setPublickeyAuthenticator(pubkeyAuthenticator)
			sshd.setPasswordAuthenticator(passwordAuthenticator)
			sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(authorizedKeys.toFile))
			sshd.setFileSystemFactory(new VirtualFileSystemFactory(homeDir.toAbsolutePath))
			sshd.setCommandFactory(new ScpCommandFactory())
			sshd.start
			println(s"sshd started: port = $portNumber, home = ${homeDir.toAbsolutePath}")
			if (!FileWaiter.waitFile(fileToWait)) sys.error(s"not watched")

		} catch {
			case NonFatal(ex) =>
				ex.printStackTrace
				throw ex

		} finally {
			println("sshd stopping " + "#" * 32)
			sshd.stop(true)
		}
	}
}

object FileWaiter {
	/** @return true if file exists */
	def waitFile(file: Path): Boolean = {
		if (Files.exists(file)) {
			println(s"file exists: $file")
			true

		} else {
			import StandardWatchEventKinds._
			val watcher = FileSystems.getDefault.newWatchService
			try {
				file.getParent.register(watcher, ENTRY_CREATE) // WATCH DIRECTORY // state is ready
				println(s"begin to watch a directory and wait a file: $file")
				Iterator.continually(watcher.take/*poll(timeoutInSec, TimeUnit.SECONDS)*/).takeWhile(_ != null).find { key =>
					val found = key.pollEvents.asScala.find { evt =>
						(key.watchable, evt.kind, Files.exists(file)) match {
							case (_: Path, ENTRY_CREATE, true) => // MATCH FILE // break
								println(s"catch event: $file")
								true
							case (x @ _, y @ _, z @ _) =>
								println(s"ignore event: watchable = $x, kind = $y, exists = $z")
								false // NOP
						}
					}
					key.reset // state is back to ready
					found.nonEmpty // break loop
				}.nonEmpty
			} finally watcher.close
		}
	}
}
