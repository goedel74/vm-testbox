# testbox


***


## Required

* Internet
* Git
* JDK
* VirtualBox
* sbt
* Jenkins


***


## Instruction

* git: clone
* sbt: download a single compressed file and decompress it
* Jenkins2: download a single war file ＆ Jenkinsfile をジョブ登録
* Virtual Machine を取得 あるいは [VirtualBox.md](doc/VirtualBox.md) に従って Virtual Machine を作成


***


## Roadmap

* lighten vm ...
* Jenkins
	* enable stage("prepare satellite")
		* introduce Prometheus
		* introduce Kubenetes
	* enable stage("prepare vm")
		* vm to export, import, register, and touch $vmPreparedFile
	* multiple selection : vm, jre, and acrobat reader
* pdf を出力する vm と、それを検証する vm は別個にする
	* サーバ機能を実行する vm とクライアント機能を持つ vm を別にする、ということ
	* 実運用のユースケースに近い
	* 検証は RPA 的発想で考える
	* 結果はいくつかの視点を想定 (マネージャ、開発者、テスター、 etc.)
* prodbox: git submodule
* 起動時に自動的にプログラム実行させているので、プログラム実行しない起動をサポート
* integrate sh and bat


***


## 方針

* testbox ディレクトリ配下(と vm)で機能が完結するようにする
* 環境イメージ作成のステップと CI flow の境界を見究めて、 事前の機能提供 と 自由度確保(テスト要件の変更対応, テストパターンの追加対応) のバランスを考える
* ローカルでも(オンプレでもクラウドでも)動作させる
	* e.g. Jenkinsfile の checkout scm は NG
	* (未コミットの) 変更も検証できるような環境作り
	* Jenkins plugin は(なるべく)使用しない
* テスト環境のバラエティ(linux/windows[/mac], バージョン 等)により docker は見送り
	* テスト環境の問題がクリアされた暁には docker へ移行する、という予定を意識
	* 実行したコマンドの手順を再現可能にする (e.g. ドキュメンテーション)
	* 相当する機能が docker に存在しないような Jenkins plugin は(なるべく)使用しない
* パターン・カバレッジ
	* OS の選択 (12 種) → 異なる環境イメージを事前に用意
	* jre の選択 (3 種) → ~~alternatives (linux)~~ / set %JAVA_HOME% ~~(windows)~~
	* acrobat reader の選択 (4 種) → 異なるバージョンの acrobat reader をビルド毎にインストールやり直し
	* それ以外 → テストケースの中で吸収
		* 証明書 (5 種)
		* システムプロパティ
		* Config 設定値
		* コマンド (10 種)
		* コマンド引数
		* 入力 pdf - サイズ, 日本語ディレクトリ, 日本語ファイル名, 空白のあるファイル名
		* 重ね打ち
* キーワード
	* multiple OS / linux, windows, mac
	* multiple Java versions
	* Jenkins2
		* pipeline
		* slack 連携
	* virtual machine / VirtualBox
	* ~~scalatest~~
	* ssh / pubkey auth
	* pdf / multiple AcrobatReader versions
	* watch service

