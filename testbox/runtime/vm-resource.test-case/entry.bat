@echo off
setlocal enabledelayedexpansion
cd /d %~dp0

echo -------------------- Begin of Test -------------------- && ^
call PdfAttachFilesTest.bat %* && ^
call PdfDetachFileTest.bat %* && ^
call PdfTsExpirationListTest.bat %* && ^
call PdfTsRenewTest.bat %* && ^
call UserEditTest.bat %* && ^
echo -------------------- End of Test --------------------

timeout 30
