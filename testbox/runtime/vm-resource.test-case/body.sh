#!/bin/bash

set -xu # -e は指定されない想定; 途中で処理を停止したい場合には -e を指定

export LANG=ja_JP.utf8; date; java -version

# args
#	$1: ディレクトリ e.g. out
#		このディレクトリ配下に格納されたファイルは、結果エビデンスとしてホストに送信されます
#	$2: クラスパス
#		java コマンドの -cp で指定するための値

# 実行したいコマンドを列挙してください
# e.g. java -cp $2 SomeApp --out $1
# e.g. java -cp $2 org.scalatest.tools.Runner -u $1 -f $1/testing.log -s jp.sysart.hello.World


### UserEdit
UserEdit="java -cp $2 jp.co.seiko_p.pdfts.cli.UserEdit -conf lib/cli-config-vm.xml"

$UserEdit > $1/UserEdit-引数なし.out


### PdfSignTs

#java -cp $2 jp.co.seiko_p.pdfts.cli.PdfSignTs -conf lib/cli-config-vm.xml \
#-type ES_T -in lib/odersky.pdf -out tmp/odersky.pdf -tempDir tmp -ownerPassword secret


# PdfAttachFiles : normal
PdfAttachFiles="java -cp $2 jp.co.seiko_p.pdfts.cli.PdfAttachFiles -conf lib/cli-config-vm.xml"

$PdfAttachFiles > $1/PdfAttachFiles-添付なし.pdf < lib/odersky.pdf
$PdfAttachFiles lib/seiko.png attached.png 添付の説明です < lib/odersky.pdf > $1/PdfAttachFiles-添付-日本語説明.pdf
$PdfAttachFiles lib/seiko.png attached.png "This is a comment" < lib/odersky.pdf > $1/PdfAttachFiles-添付-EnglishDesc.pdf
$PdfAttachFiles -out $1/PdfAttachFiles-添付1個.pdf -in lib/odersky.pdf lib/seiko.png attached.png "PdfDetachFile テストの入力を作るための PdfAttachFiles 実行です"

# PdfAttachFiles : abnormal


# PdfDetachFile : normal
PdfDetachFile="java -cp $2 jp.co.seiko_p.pdfts.cli.PdfDetachFile"

$PdfDetachFile -outDir $1 -in $1/PdfAttachFiles-添付1個.pdf # ファイル名は seiko.png ではなく attached.png になる

# TODO: outDir に存在しないディレクトリを指定してもエラーにならない＆ファイルが出力されていない？？？


# PdfDetachFile : abnormal

$PdfDetachFile || echo "expected"
