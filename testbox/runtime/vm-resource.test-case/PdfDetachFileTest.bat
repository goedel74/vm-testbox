@echo off
setlocal enabledelayedexpansion
cd /d %~dp0

set PdfDetachFile=java jp.co.seiko_p.pdfts.cli.PdfDetachFile
set PdfAttachFiles=java jp.co.seiko_p.pdfts.cli.PdfAttachFiles
set CONF_DIR=conf
set RESOURCE_DIR=test-resource
set OUT_DIR=%1/PdfDetachFile
mkdir %1\PdfDetachFile
set EC=exitCode/PdfDetachFile
mkdir exitCode\PdfDetachFile

@rem
@rem 環境変数 CLASSPATH は呼び出し元で設定されています。
@rem

echo ++++++++++++++++++++ PdfDetachFile Test: 001_002 引数なし ++++++++++++++++++++
%PdfDetachFile% ^
 > %OUT_DIR%/001_002-HikisuNashi.out 2>&1 ^
 && echo failure > %EC%/failure.001_002 || echo expected > %EC%/expected.001_002

echo ++++++++++++++++++++ PdfDetachFile Test: 003_004_005_006_007_008 添付ファイル 1 個を取り出し ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %RESOURCE_DIR%/odersky-signed-pre-attached.pdf ^
 > %OUT_DIR%/003_004_005_006_007_008.out 2>&1 ^
 && echo expected > %EC%/expected.003_004_005_006_007_008 || echo failure > %EC%/failure.003_004_005_006_007_008
copy "%OUT_DIR%\PdfDetachFile\pre-attachement.png" "%OUT_DIR%\PdfDetachFile\attached-003_004_005_006_007_008.png"

echo ++++++++++++++++++++ PdfDetachFile Test: 009_010 ファイルが添付されていない pdf から Detach を試みる ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %RESOURCE_DIR%/odersky-signed.pdf ^
 > %OUT_DIR%/009_010.out 2>&1 ^
 && echo expected > %EC%/expected.009_010 || echo failure > %EC%/failure.009_010

echo ++++++++++++++++++++ PdfDetachFile Test: 011_012_013_014_015_016_017 複数のファイルが添付されている場合 ++++++++++++++++++++
@rem 添付もテストに含める (PdfAttachFiles に相当するテストケースがないので。)
%PdfAttachFiles% ^
 -in %RESOURCE_DIR%/odersky-signed.pdf -out %OUT_DIR%/011_012_013_014_015_016_017-attached-10.pdf ^
 %RESOURCE_DIR%/seiko-01.txt attached-for-detach-011.txt   "This is a comment for seiko-01" ^
 %RESOURCE_DIR%/seiko-02.zip attached-for-detach-011.zip   "This is a comment for seiko-02" ^
 %RESOURCE_DIR%/seiko-03.mp3 attached-for-detach-011.mp3   "This is a comment for seiko-03" ^
 %RESOURCE_DIR%/seiko-04.mp4 attached-for-detach-011.mp4   "This is a comment for seiko-04 ／ 大きいファイルサイズの添付テストを含む" ^
 %RESOURCE_DIR%/seiko-05.png attached-for-detach-011-a.png "05.png を a.png として指定 ／ 同名ファイル名のテストを含む" ^
 %RESOURCE_DIR%/seiko-06.png attached-for-detach-011-a.png "06.png を a.png として指定 ／ 同名ファイル名のテストを含む" ^
 %RESOURCE_DIR%/seiko-07.png attached-for-detach-011-b.png "07.png を b.png として指定 ／ 同名ファイル名のテストを含む" ^
 %RESOURCE_DIR%/seiko-08.png attached-for-detach-011-b.png "08.png を b.png として指定 ／ 同名ファイル名のテストを含む" ^
 %RESOURCE_DIR%/seiko-09.png attached-for-detach-011-b.png "09.png を b.png として指定 ／ 同名ファイル名のテストを含む" ^
 %RESOURCE_DIR%/添付-10.png  添付-for-detach-011-10.png    "添付-10.png のコメント"

%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %OUT_DIR%/011_012_013_014_015_016_017-attached-10.pdf ^
 > %OUT_DIR%/011_012_013_014_015_016_017.out 2>&1 ^
 && echo expected > %EC%/expected.011_012_013_014_015_016_017 || echo failure > %EC%/failure.011_012_013_014_015_016_017

echo ++++++++++++++++++++ PdfDetachFile Test: 018_019_020 ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %RESOURCE_DIR%/バインダー1.pdf ^
 > %OUT_DIR%/018_019_020.out 2>&1 ^
 && echo expected > %EC%/expected.018_019_020 || echo failure > %EC%/failure.018_019_020

echo ++++++++++++++++++++ PdfDetachFile Test: 021_022_023_024 ディレクトリを -in に指定 ++++++++++++++++++++
mkdir "%OUT_DIR%/subdir-detach-021_022_023_024"
%PdfAttachFiles% -in %RESOURCE_DIR%/odersky-signed.pdf -out %OUT_DIR%/subdir-detach-021_022_023_024/seiko-01.pdf ^
 %RESOURCE_DIR%/seiko-01.txt attached-for-detach-021a.txt "This is a comment"
%PdfAttachFiles% -in %RESOURCE_DIR%/odersky-signed.pdf -out %OUT_DIR%/subdir-detach-021_022_023_024/seiko-02.pdf ^
 %RESOURCE_DIR%/seiko-02.zip attached-for-detach-021b.zip "This is a comment"
%PdfAttachFiles% -in %RESOURCE_DIR%/odersky-signed.pdf -out %OUT_DIR%/subdir-detach-021_022_023_024/seiko-03.pdf ^
 %RESOURCE_DIR%/seiko-03.mp3 attached-for-detach-021c.mp3 "This is a comment"
%PdfAttachFiles% -in %RESOURCE_DIR%/odersky-signed.pdf -out %OUT_DIR%/subdir-detach-021_022_023_024/seiko-04.pdf ^
 %RESOURCE_DIR%/seiko-04.mp4 attached-for-detach-021d.mp4 "This is a comment"
%PdfAttachFiles% -in %RESOURCE_DIR%/odersky-signed.pdf -out %OUT_DIR%/subdir-detach-021_022_023_024/seiko-05.pdf ^
 %RESOURCE_DIR%/seiko-05.png attached-for-detach-021e.png "This is a comment"

%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %OUT_DIR%/subdir-detach-021_022_023_024 ^
 > %OUT_DIR%/021_022_023_024.out 2>&1 ^
 && echo expected > %EC%/expected.021_022_023_024 || echo failure > %EC%/failure.021_022_023_024

echo ++++++++++++++++++++ PdfDetachFile Test: 025_026 階層のあるディレクトリを -in に指定 ++++++++++++++++++++
mkdir "%OUT_DIR%/subdir-detach-025_026"
%PdfDetachFile% -outDir %OUT_DIR%/subdir-detach-025_026 -in %RESOURCE_DIR%/subdir-detach-025_026 ^
 > %OUT_DIR%/025_026.out 2>&1 ^
 && echo expected > %EC%/expected.025_026 || echo failure > %EC%/failure.025_026

echo ++++++++++++++++++++ PdfDetachFile Test: 027_028_029_030 日本語の添付ファイル名 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/添付-10.png 日本語の添付ファイル名-attached-for-detach-027_028_029_030.png "日本語の添付ファイル名" ^
 -in %RESOURCE_DIR%/odersky-signed.pdf -out %OUT_DIR%/027_028_029_030.pdf
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %OUT_DIR%/027_028_029_030.pdf ^
 > %OUT_DIR%/027_028_029_030.out 2>&1 ^
 && echo expected > %EC%/expected.027_028_029_030 || echo failure > %EC%/failure.027_028_029_030

echo ++++++++++++++++++++ PdfDetachFile Test: 031_032 存在しないディレクトリを -out に指定 ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %RESOURCE_DIR%/not-exists-dir ^
 > %OUT_DIR%/031_032.out 2>&1 ^
 && echo failure > %EC%/failure.031_032 || echo expected > %EC%/expected.031_032

echo ++++++++++++++++++++ PdfDetachFile Test: 033_034 存在するディレクトリを -out に指定 ++++++++++++++++++++
echo 本テストの目的を果たす他のテストケースがすでに実施されている > %EC%/skip.033_034

echo ++++++++++++++++++++ PdfDetachFile Test: 035_036 ディレクトリ名が日本語 ++++++++++++++++++++
mkdir "%OUT_DIR%/ディレクトリ名が日本語"
%PdfDetachFile% -outDir %OUT_DIR%/ディレクトリ名が日本語 -in %RESOURCE_DIR%/odersky-signed-pre-attached.pdf ^
 > %OUT_DIR%/035_036.out 2>&1 ^
 && echo expected > %EC%/expected.035_036 || echo failure > %EC%/failure.035_036

echo ++++++++++++++++++++ PdfDetachFile Test: 037_038 引数 -in に何も指定しない ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in ^
 > %OUT_DIR%/037_038.out 2>&1 ^
 && echo failure > %EC%/failure.037_038 || echo expected > %EC%/expected.037_038

echo ++++++++++++++++++++ PdfDetachFile Test: 039_040 引数 -outDir に何も指定しない ++++++++++++++++++++
%PdfDetachFile% -outDir -in %RESOURCE_DIR%/odersky-signed.pdf ^
 > %OUT_DIR%/039_040.out 2>&1 ^
 && echo failure > %EC%/failure.039_040 || echo expected > %EC%/expected.039_040

echo ++++++++++++++++++++ PdfDetachFile Test: 041_042 引数 -outDir に既存ファイル名を指定 ++++++++++++++++++++
%PdfDetachFile% -outDir %RESOURCE_DIR%/odersky-signed.pdf -in %RESOURCE_DIR%/odersky-signed.pdf ^
 > %OUT_DIR%/041_042.out 2>&1 ^
 && echo failure > %EC%/failure.041_042 || echo expected > %EC%/expected.041_042

echo ++++++++++++++++++++ PdfDetachFile Test: 043_044 引数 -in の pdf が 0-byte ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %RESOURCE_DIR%/empty.pdf ^
 > %OUT_DIR%/043_044.out 2>&1 ^
 && echo failure > %EC%/failure.043_044 || echo expected > %EC%/expected.043_044

echo ++++++++++++++++++++ PdfDetachFile Test: 045 パスワード(読み取り禁止) ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %1/PdfAttachFiles/010.pdf -ownerPassword seiko ^
 > %OUT_DIR%/045.out 2>&1 ^
 && echo expected > %EC%/expected.045 || echo failure > %EC%/failure.045

echo ++++++++++++++++++++ PdfDetachFile Test: 046 パスワード(コピー禁止) ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %1/PdfAttachFiles/011.pdf -ownerPassword seiko ^
 > %OUT_DIR%/046.out 2>&1 ^
 && echo expected > %EC%/expected.046 || echo failure > %EC%/failure.046

echo ++++++++++++++++++++ PdfDetachFile Test: 047 パスワード(印刷禁止) ++++++++++++++++++++
%PdfDetachFile% -outDir %OUT_DIR%\PdfDetachFile -in %1/PdfAttachFiles/012.pdf -ownerPassword seiko ^
 > %OUT_DIR%/047.out 2>&1 ^
 && echo expected > %EC%/expected.047 || echo failure > %EC%/failure.047

echo ++++++++++++++++++++ PdfDetachFile Test: 048 引数 -help の確認 ++++++++++++++++++++
%PdfDetachFile% -help ^
 > %OUT_DIR%/048-help.out 2>&1 ^
 && echo failure > %EC%/failure.048 || echo expected > %EC%/expected.048

exit /b 0
