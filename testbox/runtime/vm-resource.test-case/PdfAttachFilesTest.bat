@echo off
setlocal enabledelayedexpansion
cd /d %~dp0

set PdfAttachFiles=java jp.co.seiko_p.pdfts.cli.PdfAttachFiles
set CONF_DIR=conf
set RESOURCE_DIR=test-resource
set OUT_DIR=%1/PdfAttachFiles
mkdir %1\PdfAttachFiles
set EC=exitCode/PdfAttachFiles
mkdir exitCode\PdfAttachFiles

@rem
@rem 環境変数 CLASSPATH は呼び出し元で設定されています。
@rem

echo ++++++++++++++++++++ PdfAttachFiles Test: 001 引数なし ++++++++++++++++++++
%PdfAttachFiles% > %OUT_DIR%/001-HikisuNashi.pdf < %RESOURCE_DIR%/odersky-unsigned.pdf ^
 && echo expected > %EC%/expected.001 || echo failure > %EC%/failure.001

echo ++++++++++++++++++++ PdfAttachFiles Test: 002_003 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-002_003.png "最小の使用方法" < %RESOURCE_DIR%/odersky-unsigned.pdf > %OUT_DIR%/002_003-Tenpu-EnglishDesc.pdf ^
 && echo expected > %EC%/expected.002_003 || echo failure > %EC%/failure.002_003

echo ++++++++++++++++++++ PdfAttachFiles Test: 004_005 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-004_005.png "引数指定 in, out" -out %OUT_DIR%/004_005-Tenpu-1.pdf -in %RESOURCE_DIR%/odersky-unsigned.pdf ^
 > %OUT_DIR%/004_005.out 2>&1 ^
 && echo expected > %EC%/expected.004_005 || echo failure > %EC%/failure.004_005

echo ++++++++++++++++++++ PdfAttachFiles Test: 006_007_008 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-006_007_008.png "引数指定 conf, login*" -out %OUT_DIR%/006_007_008.pdf -in %RESOURCE_DIR%/odersky-unsigned.pdf ^
 -conf %CONF_DIR%/cli-config-tmp.xml -loginId ryota-2@sysart.jp -loginPassword 9eEn249Le ^
 > %OUT_DIR%/006_007_008.out 2>&1 ^
 && echo expected > %EC%/expected.006_007_008 || echo failure > %EC%/failure.006_007_008

echo ++++++++++++++++++++ PdfAttachFiles Test: 009 ++++++++++++++++++++
%PdfAttachFiles% -conf %RESOURCE_DIR%/cli-alien.xml %RESOURCE_DIR%/seiko.png attached-009.png "存在しない conf を指定" < %RESOURCE_DIR%/odersky-unsigned.pdf ^
 > %OUT_DIR%/009.out 2>&1 ^
 && echo failure > %EC%/failure.009 || echo expected > %EC%/expected.009

echo ++++++++++++++++++++ PdfAttachFiles Test: 010 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-010.png "パスワード(読み取り禁止)" -out %OUT_DIR%/010.pdf -in %RESOURCE_DIR%/noread20150616.pdf -ownerPassword seiko ^
 > %OUT_DIR%/010.out 2>&1 ^
 && echo expected > %EC%/expected.010 || echo failure > %EC%/failure.010

echo ++++++++++++++++++++ PdfAttachFiles Test: 011 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-011.png "パスワード(コピー禁止)" > %OUT_DIR%/011.pdf < %RESOURCE_DIR%/nocopy20150616.pdf -ownerPassword seiko ^
 && echo expected > %EC%/expected.011 || echo failure > %EC%/failure.011

echo ++++++++++++++++++++ PdfAttachFiles Test: 012 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-012.png "パスワード(印刷禁止)" > %OUT_DIR%/012.pdf < %RESOURCE_DIR%/noprint20150616.pdf -ownerPassword seiko ^
 && echo expected > %EC%/expected.012 || echo failure > %EC%/failure.012

echo ++++++++++++++++++++ PdfAttachFiles Test: 013_014_015_016 ++++++++++++++++++++
%PdfAttachFiles% -out %OUT_DIR%/013_014_015_016-Tenpu-1.pdf -in %RESOURCE_DIR%/odersky-unsigned.pdf %RESOURCE_DIR%/seiko.png attached-013_014_015_016.png "引数指定 proxy*" ^
 -proxyHost 172.24.64.95 -proxyPort 13128 -proxyUser sysart-squid -proxyPassword 9eEn249Le ^
 > %OUT_DIR%/013_014_015_016.out 2>&1 ^
 && echo expected > %EC%/expected.013_014_015_016 || echo failure > %EC%/failure.013_014_015_016

echo ++++++++++++++++++++ PdfAttachFiles Test: 017 ++++++++++++++++++++
echo -useNopSslHostnameVerifier 指定は実施しない on 2018-06 > %EC%/skip.017

echo ++++++++++++++++++++ PdfAttachFiles Test: 018 ++++++++++++++++++++
@rem 管理 GUI から、台紙「whitebase」が(ユーザに紐付けられた形で)登録済みであることが前提
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-018.png "テンプレート" -out %OUT_DIR%/018.pdf -template whitebase ^
 > %OUT_DIR%/018.out 2>&1 ^
 && echo expected > %EC%/expected.018 || echo failure > %EC%/failure.018

echo ++++++++++++++++++++ PdfAttachFiles Test: 019 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko.png attached-019.png "不正なテンプレート" -out %OUT_DIR%/019.pdf -template alien ^
 > %OUT_DIR%/019.out 2>&1 ^
 && echo failure > %EC%/failure.019 || echo expected > %EC%/expected.019

echo ++++++++++++++++++++ PdfAttachFiles Test: 020 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/seiko-alien.png attached-020.png "存在しないファイルを添付指定" -in %RESOURCE_DIR%/odersky-unsigned.pdf -out %OUT_DIR%/020.pdf ^
 > %OUT_DIR%/020.out 2>&1 ^
 && echo failure > %EC%/failure.020 || echo expected > %EC%/expected.020

echo ++++++++++++++++++++ PdfAttachFiles Test: 021_022 ++++++++++++++++++++
%PdfAttachFiles% %RESOURCE_DIR%/mlky_6.mp4 attached-021_022.mp4 "大きいファイルサイズの添付" -in %RESOURCE_DIR%/odersky-unsigned.pdf -out %OUT_DIR%/021_022.pdf ^
 > %OUT_DIR%/021_022.out 2>&1 ^
 && echo expected > %EC%/expected.021_022 || echo failure > %EC%/failure.021_022

echo ++++++++++++++++++++ PdfAttachFiles Test: 023_024 ++++++++++++++++++++
%PdfAttachFiles% -in %RESOURCE_DIR%/odersky-unsigned.pdf -out %OUT_DIR%/023_024-threshold.pdf ^
 test-resource/attach128/f001.txt  f001.txt  "001" ^
 test-resource/attach128/f002.txt  f002.txt  "002" ^
 test-resource/attach128/f003.txt  f003.txt  "003" ^
 test-resource/attach128/f004.txt  f004.txt  "004" ^
 test-resource/attach128/f005.txt  f005.txt  "005" ^
 test-resource/attach128/f006.txt  f006.txt  "006" ^
 test-resource/attach128/f007.txt  f007.txt  "007" ^
 test-resource/attach128/f008.txt  f008.txt  "008" ^
 test-resource/attach128/f009.txt  f009.txt  "009" ^
 test-resource/attach128/f010.txt  f010.txt  "010" ^
 test-resource/attach128/f011.txt  f011.txt  "011" ^
 test-resource/attach128/f012.txt  f012.txt  "012" ^
 test-resource/attach128/f013.txt  f013.txt  "013" ^
 test-resource/attach128/f014.txt  f014.txt  "014" ^
 test-resource/attach128/f015.txt  f015.txt  "015" ^
 test-resource/attach128/f016.txt  f016.txt  "016" ^
 test-resource/attach128/f017.txt  f017.txt  "017" ^
 test-resource/attach128/f018.txt  f018.txt  "018" ^
 test-resource/attach128/f019.txt  f019.txt  "019" ^
 test-resource/attach128/f020.txt  f020.txt  "020" ^
 test-resource/attach128/f021.log  f021.log  "021" ^
 test-resource/attach128/f022.log  f022.log  "022" ^
 test-resource/attach128/f023.log  f023.log  "023" ^
 test-resource/attach128/f024.log  f024.log  "024" ^
 test-resource/attach128/f025.log  f025.log  "025" ^
 test-resource/attach128/f026.log  f026.log  "026" ^
 test-resource/attach128/f027.log  f027.log  "027" ^
 test-resource/attach128/f028.log  f028.log  "028" ^
 test-resource/attach128/f029.log  f029.log  "029" ^
 test-resource/attach128/f030.log  f030.log  "030" ^
 test-resource/attach128/f031.log  f031.log  "031" ^
 test-resource/attach128/f032.log  f032.log  "032" ^
 test-resource/attach128/f033.log  f033.log  "033" ^
 test-resource/attach128/f034.log  f034.log  "034" ^
 test-resource/attach128/f035.log  f035.log  "035" ^
 test-resource/attach128/f036.log  f036.log  "036" ^
 test-resource/attach128/f037.log  f037.log  "037" ^
 test-resource/attach128/f038.log  f038.log  "038" ^
 test-resource/attach128/f039.log  f039.log  "039" ^
 test-resource/attach128/f040.log  f040.log  "040" ^
 test-resource/attach128/f041      f041      "041" ^
 test-resource/attach128/f042      f042      "042" ^
 test-resource/attach128/f043      f043      "043" ^
 test-resource/attach128/f044      f044      "044" ^
 test-resource/attach128/f045      f045      "045" ^
 test-resource/attach128/f046      f046      "046" ^
 test-resource/attach128/f047      f047      "047" ^
 test-resource/attach128/f048      f048      "048" ^
 test-resource/attach128/f049      f049      "049" ^
 test-resource/attach128/f050      f050      "050" ^
 test-resource/attach128/f051      f051      "051" ^
 test-resource/attach128/f052      f052      "052" ^
 test-resource/attach128/f053      f053      "053" ^
 test-resource/attach128/f054      f054      "054" ^
 test-resource/attach128/f055      f055      "055" ^
 test-resource/attach128/f056      f056      "056" ^
 test-resource/attach128/f057      f057      "057" ^
 test-resource/attach128/f058      f058      "058" ^
 test-resource/attach128/f059      f059      "059" ^
 test-resource/attach128/f060      f060      "060" ^
 test-resource/attach128/f061.jpeg f061.jpeg "061" ^
 test-resource/attach128/f062.jpeg f062.jpeg "062" ^
 test-resource/attach128/f063.jpeg f063.jpeg "063" ^
 test-resource/attach128/f064.jpeg f064.jpeg "064" ^
 test-resource/attach128/f065.jpeg f065.jpeg "065" ^
 test-resource/attach128/f066.jpeg f066.jpeg "066" ^
 test-resource/attach128/f067.jpeg f067.jpeg "067" ^
 test-resource/attach128/f068.jpeg f068.jpeg "068" ^
 test-resource/attach128/f069.jpeg f069.jpeg "069" ^
 test-resource/attach128/f070.jpeg f070.jpeg "070" ^
 test-resource/attach128/f071.png  f071.png  "071" ^
 test-resource/attach128/f072.png  f072.png  "072" ^
 test-resource/attach128/f073.png  f073.png  "073" ^
 test-resource/attach128/f074.png  f074.png  "074" ^
 test-resource/attach128/f075.png  f075.png  "075" ^
 test-resource/attach128/f076.png  f076.png  "076" ^
 test-resource/attach128/f077.png  f077.png  "077" ^
 test-resource/attach128/f078.png  f078.png  "078" ^
 test-resource/attach128/f079.png  f079.png  "079" ^
 test-resource/attach128/f080.png  f080.png  "080" ^
 test-resource/attach128/f081.svg  f081.svg  "081" ^
 test-resource/attach128/f082.svg  f082.svg  "082" ^
 test-resource/attach128/f083.svg  f083.svg  "083" ^
 test-resource/attach128/f084.svg  f084.svg  "084" ^
 test-resource/attach128/f085.svg  f085.svg  "085" ^
 test-resource/attach128/f086.svg  f086.svg  "086" ^
 test-resource/attach128/f087.svg  f087.svg  "087" ^
 test-resource/attach128/f088.svg  f088.svg  "088" ^
 test-resource/attach128/f089.svg  f089.svg  "089" ^
 test-resource/attach128/f090.svg  f090.svg  "090" ^
 test-resource/attach128/f091.mp3  f091.mp3  "091" ^
 test-resource/attach128/f092.mp3  f092.mp3  "092" ^
 test-resource/attach128/f093.mp3  f093.mp3  "093" ^
 test-resource/attach128/f094.mp3  f094.mp3  "094" ^
 test-resource/attach128/f095.mp3  f095.mp3  "095" ^
 test-resource/attach128/f096.mp3  f096.mp3  "096" ^
 test-resource/attach128/f097.mp3  f097.mp3  "097" ^
 test-resource/attach128/f098.mp3  f098.mp3  "098" ^
 test-resource/attach128/f099.mp3  f099.mp3  "099" ^
 test-resource/attach128/f100.mp3  f100.mp3  "100" ^
 test-resource/attach128/f101.exe  f101.exe  "101" ^
 test-resource/attach128/f102.exe  f102.exe  "102" ^
 test-resource/attach128/f103.exe  f103.exe  "103" ^
 test-resource/attach128/f104.exe  f104.exe  "104" ^
 test-resource/attach128/f105.exe  f105.exe  "105" ^
 test-resource/attach128/f106.exe  f106.exe  "106" ^
 test-resource/attach128/f107.exe  f107.exe  "107" ^
 test-resource/attach128/f108.exe  f108.exe  "108" ^
 test-resource/attach128/f109.exe  f109.exe  "109" ^
 test-resource/attach128/f110.exe  f110.exe  "110" ^
 test-resource/attach128/f111.sh   f111.sh   "111" ^
 test-resource/attach128/f112.sh   f112.sh   "112" ^
 test-resource/attach128/f113.sh   f113.sh   "113" ^
 test-resource/attach128/f114.sh   f114.sh   "114" ^
 test-resource/attach128/f115.sh   f115.sh   "115" ^
 test-resource/attach128/f116.sh   f116.sh   "116" ^
 test-resource/attach128/f117.sh   f117.sh   "117" ^
 test-resource/attach128/f118.sh   f118.sh   "118" ^
 test-resource/attach128/f119.sh   f119.sh   "119" ^
 test-resource/attach128/f120.sh   f120.sh   "120" ^
 test-resource/attach128/f121.cmd  f121.cmd  "121" ^
 test-resource/attach128/f122.cmd  f122.cmd  "122" ^
 test-resource/attach128/f123.cmd  f123.cmd  "123" ^
 test-resource/attach128/f124.cmd  f124.cmd  "124" ^
 test-resource/attach128/f125.cmd  f125.cmd  "125" ^
 test-resource/attach128/f126.cmd  f126.cmd  "126" ^
 test-resource/attach128/f127.cmd  f127.cmd  "127" ^
 test-resource/attach128/f128.cmd  f128.cmd  "128" ^
 > %OUT_DIR%/023_024.out 2>&1 ^
 && echo expected > %EC%/expected.023_024 || echo failure > %EC%/failure.023_024

exit /b 0
