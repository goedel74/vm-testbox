@echo off
setlocal enabledelayedexpansion
cd /d %~dp0

set UserEdit=java jp.co.seiko_p.pdfts.cli.UserEdit
set CONF_DIR=conf
set RESOURCE_DIR=test-resource
set OUT_DIR=%1/UserEdit
mkdir %1\UserEdit
set EC=exitCode/UserEdit
mkdir exitCode\UserEdit

@rem
@rem 環境変数 CLASSPATH は呼び出し元で設定されています。
@rem

echo ++++++++++++++++++++ UserEdit Test: 001_002_003 ++++++++++++++++++++
%UserEdit% ^
 > %OUT_DIR%/001_002_003-normal-HikisuNashi.out 2>&1 ^
 && echo expected > %EC%/expected.001_002_003 || echo failure > %EC%/failure.001_002_003

echo ++++++++++++++++++++ UserEdit Test: 004 ++++++++++++++++++++
%UserEdit% -conf %CONF_DIR%/cli-config-copy.xml ^
 > %OUT_DIR%/004-normal.out 2>&1 ^
 && echo expected > %EC%/expected.004 || echo failure > %EC%/failure.004

echo ++++++++++++++++++++ UserEdit Test: 005 ++++++++++++++++++++
%UserEdit% -loginId alien -errorCommand "cmd /c copy nul %OUT_DIR%/005-error.out" ^
 && echo failure > %EC%/failure.005 || echo expected > %EC%/expected.005

echo ++++++++++++++++++++ UserEdit Test: 006_007 ++++++++++++++++++++
%UserEdit% -conf %CONF_DIR%/cli-config-tmp.xml -loginId ryota-2@sysart.jp -loginPassword 9eEn249Le ^
 > %OUT_DIR%/006_007-normal.out 2>&1 ^
 && echo expected > %EC%/expected.006_007 || echo failure > %EC%/failure.006_007

echo ++++++++++++++++++++ UserEdit Test: 008_009_010_011 ++++++++++++++++++++
%UserEdit% -proxyHost 172.24.64.95 -proxyPort 13128 -proxyUser sysart-squid -proxyPassword 9eEn249Le ^
 > %OUT_DIR%/008_009_010_011-normal.out 2>&1 ^
 && echo expected > %EC%/expected.008_009_010_011 || echo failure > %EC%/failure.008_009_010_011

echo ++++++++++++++++++++ UserEdit Test: 012 ++++++++++++++++++++
echo -useNopSslHostnameVerifier は実施しない on 2018-06 > %EC%/skip.012

echo ++++++++++++++++++++ UserEdit Test: 013 ++++++++++++++++++++
echo -useNopSslTrustManager は実施しない on 2018-06 > %EC%/skip.013

echo ++++++++++++++++++++ UserEdit Test: 014 ++++++++++++++++++++
%UserEdit% -help ^
 > %OUT_DIR%/014-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.014 || echo expected > %EC%/expected.014

echo ++++++++++++++++++++ UserEdit Test: 015 ++++++++++++++++++++
%UserEdit% パスワード=xyz ^
 > %OUT_DIR%/015-normal.out 2>&1 ^
 && echo expected > %EC%/expected.015 || echo failure > %EC%/failure.015
echo ++++++++++++++++++++ UserEdit Test: 015 (revert) ++++++++++++++++++++
%UserEdit% -loginPassword xyz パスワード=9eEn249Le

echo ++++++++++++++++++++ UserEdit Test: 016 ++++++++++++++++++++
%UserEdit% 氏=渋谷 ^
 > %OUT_DIR%/016-normal.out 2>&1 ^
 && echo expected > %EC%/expected.016 || echo failure > %EC%/failure.016

echo ++++++++++++++++++++ UserEdit Test: 017 ++++++++++++++++++++
%UserEdit% 名=太郎 ^
 > %OUT_DIR%/017-normal.out 2>&1 ^
 && echo expected > %EC%/expected.017 || echo failure > %EC%/failure.017

echo ++++++++++++++++++++ UserEdit Test: 018 ++++++++++++++++++++
%UserEdit% 氏カナ=シブヤ ^
 > %OUT_DIR%/018-normal.out 2>&1 ^
 && echo expected > %EC%/expected.018 || echo failure > %EC%/failure.018

echo ++++++++++++++++++++ UserEdit Test: 019 ++++++++++++++++++++
%UserEdit% 名カナ=タロウ ^
 > %OUT_DIR%/019-normal.out 2>&1 ^
 && echo expected > %EC%/expected.019 || echo failure > %EC%/failure.019

echo ++++++++++++++++++++ UserEdit Test: 020 ++++++++++++++++++++
%UserEdit% 組織名=あいうえお ^
 > %OUT_DIR%/020-normal.out 2>&1 ^
 && echo expected > %EC%/expected.020 || echo failure > %EC%/failure.020

echo ++++++++++++++++++++ UserEdit Test: 021 ++++++++++++++++++++
%UserEdit% 部署名=abc ^
 > %OUT_DIR%/021-normal.out 2>&1 ^
 && echo expected > %EC%/expected.021 || echo failure > %EC%/failure.021

echo ++++++++++++++++++++ UserEdit Test: 022 ++++++++++++++++++++
%UserEdit% 役職名=ＰＧ ^
 > %OUT_DIR%/022-normal.out 2>&1 ^
 && echo expected > %EC%/expected.022 || echo failure > %EC%/failure.022

echo ++++++++++++++++++++ UserEdit Test: 023 ++++++++++++++++++++
%UserEdit% 郵便番号=123-4567 ^
 > %OUT_DIR%/023-normal.out 2>&1 ^
 && echo expected > %EC%/expected.023 || echo failure > %EC%/failure.023

echo ++++++++++++++++++++ UserEdit Test: 024 ++++++++++++++++++++
%UserEdit% 都道府県id=11 ^
 > %OUT_DIR%/024-normal.out 2>&1 ^
 && echo expected > %EC%/expected.024 || echo failure > %EC%/failure.024

echo ++++++++++++++++++++ UserEdit Test: 025 ++++++++++++++++++++
%UserEdit% 市区郡町村=アイウエオ ^
 > %OUT_DIR%/025-normal.out 2>&1 ^
 && echo expected > %EC%/expected.025 || echo failure > %EC%/failure.025

echo ++++++++++++++++++++ UserEdit Test: 026 ++++++++++++++++++++
%UserEdit% 町域=あいうえお ^
 > %OUT_DIR%/026-normal.out 2>&1 ^
 && echo expected > %EC%/expected.026 || echo failure > %EC%/failure.026

echo ++++++++++++++++++++ UserEdit Test: 027 ++++++++++++++++++++
%UserEdit% 番地=１２３-5 ^
 > %OUT_DIR%/027-normal.out 2>&1 ^
 && echo expected > %EC%/expected.027 || echo failure > %EC%/failure.027

echo ++++++++++++++++++++ UserEdit Test: 028 ++++++++++++++++++++
%UserEdit% 建物=アイウエオA ^
 > %OUT_DIR%/028-normal.out 2>&1 ^
 && echo expected > %EC%/expected.028 || echo failure > %EC%/failure.028

echo ++++++++++++++++++++ UserEdit Test: 029 ++++++++++++++++++++
%UserEdit% 電話=0120-12-1234 ^
 > %OUT_DIR%/029-normal.out 2>&1 ^
 && echo expected > %EC%/expected.029 || echo failure > %EC%/failure.029

echo ++++++++++++++++++++ UserEdit Test: 030 ++++++++++++++++++++
%UserEdit% fax=0120-56-7890 ^
 > %OUT_DIR%/030-normal.out 2>&1 ^
 && echo expected > %EC%/expected.030 || echo failure > %EC%/failure.030

echo ++++++++++++++++++++ UserEdit Test: 031 ++++++++++++++++++++
%UserEdit% 備考=テスト ^
 > %OUT_DIR%/031-normal.out 2>&1 ^
 && echo expected > %EC%/expected.031 || echo failure > %EC%/failure.031

echo ++++++++++++++++++++ UserEdit Test: 032 ++++++++++++++++++++
%UserEdit% eメールアドレス=test@sysart.jp ^
 > %OUT_DIR%/032-normal.out 2>&1 ^
 && echo expected > %EC%/expected.032 || echo failure > %EC%/failure.032

echo ++++++++++++++++++++ UserEdit Test: 034 ++++++++++++++++++++
@rem Test 032 で eメールアドレス が変更されているので ログイン ID != eメールアドレス の状態
%UserEdit% ^
 > %OUT_DIR%/034-normal.out 2>&1 ^
 && echo expected > %EC%/expected.034 || echo failure > %EC%/failure.034
echo ++++++++++++++++++++ UserEdit Test: 033 ++++++++++++++++++++
@rem eメールアドレス を変更して ログイン ID == eメールアドレス の状態にする
%UserEdit% eメールアドレス=ryota-2@sysart.jp
%UserEdit% ^
 > %OUT_DIR%/033-normal.out 2>&1 ^
 && echo expected > %EC%/expected.033 || echo failure > %EC%/failure.033

echo ++++++++++++++++++++ UserEdit Test: 035 ++++++++++++++++++++
@rem 変更されないことを確認 → (変更されないはずなので) revert しない
%UserEdit% ログインid=test0101 ^
 > %OUT_DIR%/035-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.035 || echo expected > %EC%/expected.035

echo ++++++++++++++++++++ UserEdit Test: 036_037 ++++++++++++++++++++
%UserEdit% 氏= ^
 > %OUT_DIR%/036_037-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.036_037 || echo expected > %EC%/expected.036_037

echo ++++++++++++++++++++ UserEdit Test: 038 ++++++++++++++++++++
%UserEdit% 名= ^
 > %OUT_DIR%/038-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.038 || echo expected > %EC%/expected.038

echo ++++++++++++++++++++ UserEdit Test: 039 ++++++++++++++++++++
%UserEdit% 氏カナ=てすと ^
 > %OUT_DIR%/039-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.039 || echo expected > %EC%/expected.039

echo ++++++++++++++++++++ UserEdit Test: 040 ++++++++++++++++++++
%UserEdit% 名カナ=test ^
 > %OUT_DIR%/040-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.040 || echo expected > %EC%/expected.040

echo ++++++++++++++++++++ UserEdit Test: 041 ++++++++++++++++++++
%UserEdit% 組織名= ^
 > %OUT_DIR%/041-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.041 || echo expected > %EC%/expected.041

echo ++++++++++++++++++++ UserEdit Test: 042 ++++++++++++++++++++
%UserEdit% 部署名= ^
 > %OUT_DIR%/042-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.042 || echo expected > %EC%/expected.042

echo ++++++++++++++++++++ UserEdit Test: 043 ++++++++++++++++++++
%UserEdit% 役職名=123456789012345678901234567890123456789012345678901 ^
 > %OUT_DIR%/043-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.043 || echo expected > %EC%/expected.043

echo ++++++++++++++++++++ UserEdit Test: 044 ++++++++++++++++++++
%UserEdit% 郵便番号=0123456 ^
 > %OUT_DIR%/044-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.044 || echo expected > %EC%/expected.044

echo ++++++++++++++++++++ UserEdit Test: 045 ++++++++++++++++++++
%UserEdit% 市区郡町村= ^
 > %OUT_DIR%/045-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.045 || echo expected > %EC%/expected.045

echo ++++++++++++++++++++ UserEdit Test: 046 ++++++++++++++++++++
%UserEdit% 町域=あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもabcde12345678901 ^
 > %OUT_DIR%/046-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.046 || echo expected > %EC%/expected.046

echo ++++++++++++++++++++ UserEdit Test: 047 ++++++++++++++++++++
%UserEdit% 番地= ^
 > %OUT_DIR%/047-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.047 || echo expected > %EC%/expected.047

echo ++++++++++++++++++++ UserEdit Test: 048 ++++++++++++++++++++
%UserEdit% 建物=アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモ-----ABCDEFGHIJK ^
 > %OUT_DIR%/048-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.048 || echo expected > %EC%/expected.048

echo ++++++++++++++++++++ UserEdit Test: 049 ++++++++++++++++++++
%UserEdit% 電話=0123 ^
 > %OUT_DIR%/049-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.049 || echo expected > %EC%/expected.049

echo ++++++++++++++++++++ UserEdit Test: 050 ++++++++++++++++++++
%UserEdit% fax=0123-56 ^
 > %OUT_DIR%/050-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.050 || echo expected > %EC%/expected.050

echo ++++++++++++++++++++ UserEdit Test: 051 ++++++++++++++++++++
%UserEdit% 備考=あいうえお-----かきくけこ@@@@@さしすせそ_____たちつてと/////なにぬねの=====１２３４５６７８９０1234567890abcdefghijABCDEFGHIJＡＢＣＤＥＦＧＨＩＪあいうえお-----かきくけこ@@@@@さしすせそ_____たちつてと/////なにぬねの=====１２３４５６７８９０1234567890abcdefghijABCDEFGHIJＡＢＣＤＥＦＧＨＩＪこの文の最後の句点が219文字目です。この文の最後の句点が238文字目です。この文の最後の文字が256文字目です ^
 > %OUT_DIR%/051-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.051 || echo expected > %EC%/expected.051

echo ++++++++++++++++++++ UserEdit Test: 052 ++++++++++++++++++++
%UserEdit% eメールアドレス=test567890123456789012345678901234567890A@sysart.jp ^
 > %OUT_DIR%/052-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.052 || echo expected > %EC%/expected.052

echo ++++++++++++++++++++ UserEdit Test: 053 ++++++++++++++++++++
%UserEdit% eメールアドレス=test.sysart.jp ^
 > %OUT_DIR%/053-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.053 || echo expected > %EC%/expected.053

echo ++++++++++++++++++++ UserEdit Test: 054 ++++++++++++++++++++
%UserEdit% eメールアドレス=test@@sysart.jp ^
 > %OUT_DIR%/054-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.054 || echo expected > %EC%/expected.054

echo ++++++++++++++++++++ UserEdit Test: 055 ++++++++++++++++++++
%UserEdit% eメールアドレス=foo@sysart.jp ^
 > %OUT_DIR%/055-normal.out 2>&1 ^
 && echo expected > %EC%/expected.055 || echo failure > %EC%/failure.055

echo ++++++++++++++++++++ UserEdit Test: 056 ++++++++++++++++++++
%UserEdit% -loginPassword alien 氏=会員 ^
 > %OUT_DIR%/056-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.056 || echo expected > %EC%/expected.056

echo ++++++++++++++++++++ UserEdit Test: 057 ++++++++++++++++++++
echo ログ確認後、現状(2018-06)はブラウザで https://timestamp.seiko-cybertime.jp/UserEdit ページを開いて目視確認 (下の Test 60 を実施せずに確認) > %EC%/skip.057

echo ++++++++++++++++++++ UserEdit Test: 058 ++++++++++++++++++++
%UserEdit% -loginId ryota@sysart.jp -loginPassword 9eEn249Le ^
 > %OUT_DIR%/058-normal.out 2>&1 ^
 && echo expected > %EC%/expected.058 || echo failure > %EC%/failure.058

echo ++++++++++++++++++++ UserEdit Test: 059 ++++++++++++++++++++
@rem (「サーバーが立ち上がっていない状態」については、いくつかの段階がある)
@rem (1) インターネットに繋がっていない
@rem (2) DNS が解決しない
@rem (3) DNS が解決 → ping が通らない
@rem (4) DNS が解決 → ping が通る → port が開いていない
@rem (5) DNS が解決 → ping が通る → port が開いている → 404 が返ってくる
@rem
@rem DNS が解決しない場合
%UserEdit% -conf %CONF_DIR%/cli-config-illegal-baseUrl.xml ^
 > %OUT_DIR%/059-abnormal.out 2>&1 ^
 && echo failure > %EC%/failure.059 || echo expected > %EC%/expected.059

echo ++++++++++++++++++++ UserEdit Test: 060 ++++++++++++++++++++
%UserEdit% ^
 氏=山田 ^
 名=花子 ^
 氏カナ=ヤマダ ^
 名カナ=ハナコ ^
 組織名=株式会社日本 ^
 部署名=開発 ^
 役職名=次郎長 ^
 郵便番号=999-9999 ^
 市区郡町村=横浜市 ^
 町域=となり町 ^
 番地=コンビニ前 ^
 建物=○○ビル ^
 電話=03-1111-2222 ^
 fax=03-3333-4444 ^
 備考=びこう ^
 eメールアドレス=ryota-2@sysart.jp ^
 > %OUT_DIR%/060-normal.out 2>&1 ^
 && echo expected > %EC%/expected.060 || echo failure > %EC%/failure.060

exit /b 0
