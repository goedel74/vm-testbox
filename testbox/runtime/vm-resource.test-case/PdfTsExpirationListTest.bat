@echo off
setlocal enabledelayedexpansion
cd /d %~dp0

set PdfTsExpirationList=java jp.co.seiko_p.pdfts.cli.PdfTsExpirationList
set CONF_DIR=conf
set RESOURCE_DIR=test-resource
set OUT_DIR=%1/PdfTsExpirationList
mkdir %1\PdfTsExpirationList
set EC=exitCode/PdfTsExpirationList
mkdir exitCode\PdfTsExpirationList

@rem
@rem 環境変数 CLASSPATH は呼び出し元で設定されています。
@rem

echo ++++++++++++++++++++ PdfTsExpirationList Test: 001_002 引数なし ++++++++++++++++++++
@rem カレントディレクトリが対象 → copy-execute-delete 方式
mkdir watch-out
copy %RESOURCE_DIR%\odersky-signed.pdf .
copy %RESOURCE_DIR%\PdfTsExpirationList\odersky-signed-ES_A-ES_T-ES_A-DTS.pdf .

%PdfTsExpirationList% ^
 > %OUT_DIR%/001_002.out 2>&1 ^
 && echo expected > %EC%/expected.001_002 || echo failure > %EC%/failure.001_002

del /Q .\odersky-signed.pdf
del /Q .\odersky-signed-ES_A-ES_T-ES_A-DTS.pdf
rmdir /Q watch-out

echo ++++++++++++++++++++ PdfTsExpirationList Test: 003_004 引数指定 in(directory), outFile ++++++++++++++++++++
%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList -outFile %OUT_DIR%/003_004.txt ^
 > %OUT_DIR%/003_004.out 2>&1 ^
 && echo expected > %EC%/expected.003_004 || echo failure > %EC%/failure.003_004

echo ++++++++++++++++++++ PdfTsExpirationList Test: 005 引数指定 in(directory), outFile, conf ++++++++++++++++++++
%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList -outFile %OUT_DIR%/005.txt ^
 -conf %CONF_DIR%/cli-config-copy.xml ^
 > %OUT_DIR%/005.out 2>&1 ^
 && echo expected > %EC%/expected.005 || echo failure > %EC%/failure.005

echo ++++++++++++++++++++ PdfTsExpirationList Test: 006 引数指定 errorCommand ++++++++++++++++++++
@rem 未署名の pdf を与えることで key not found のエラーを意図的に起こす
%PdfTsExpirationList% -in %RESOURCE_DIR%/odersky-unsigned.pdf ^
 -errorCommand "cmd /c copy nul %OUT_DIR%/006-error.txt" ^
 && echo failure > %EC%/failure.006 || echo expected > %EC%/expected.006

echo ++++++++++++++++++++ PdfTsExpirationList Test: 007 パスワード付き pdf ++++++++++++++++++++
@rem (データ生成)
@rem PdfSignTs -type ES_A -in noread20150616.pdf -out noread-ES_A.pdf -ownerPassword seiko
%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList-pwd/noread-ES_A.pdf -outFile %OUT_DIR%/007-ES_A.txt ^
 -ownerPassword seiko ^
 > %OUT_DIR%/007-ES_A.out 2>&1 ^
 && echo expected > %EC%/expected.007 || echo failure > %EC%/failure.007

echo ++++++++++++++++++++ PdfTsExpirationList Test: 008 パスワード付き pdf ++++++++++++++++++++
@rem (データ生成)
@rem PdfSignTs -type ES_A -in nocopy20150616.pdf -out nocopy-ES_A.pdf -ownerPassword seiko
%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList-pwd/nocopy-ES_A.pdf -outFile %OUT_DIR%/008-ES_A.txt ^
 -ownerPassword seiko ^
 > %OUT_DIR%/008-ES_A.out 2>&1 ^
 && echo expected > %EC%/expected.008 || echo failure > %EC%/failure.008

echo ++++++++++++++++++++ PdfTsExpirationList Test: 009 パスワード付き pdf ++++++++++++++++++++
@rem (データ生成)
@rem PdfSignTs -type ES_A -in noprint20150616.pdf -out noprint-ES_A.pdf -ownerPassword seiko
%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList-pwd/noprint-ES_A.pdf -outFile %OUT_DIR%/009-ES_A.txt ^
 -ownerPassword seiko ^
 > %OUT_DIR%/009-ES_A.out 2>&1 ^
 && echo expected > %EC%/expected.009 || echo failure > %EC%/failure.009

echo ++++++++++++++++++++ PdfTsExpirationList Test: 010 help ++++++++++++++++++++
%PdfTsExpirationList% -help ^
 > %OUT_DIR%/010.out 2>&1 ^
 && echo failure > %EC%/failure.010 || echo expected > %EC%/expected.010

echo ++++++++++++++++++++ PdfTsExpirationList Test: 011 in(file; 重ね打ち), outFile ++++++++++++++++++++
@rem (データ生成)
@rem PdfSignTs -type ES_A -in odersky-unsigned.pdf              -out odersky-signed-ES_A.pdf
@rem PdfSignTs -type ES_T -in odersky-signed-ES_A.pdf           -out odersky-signed-ES_A-ES_T.pdf
@rem PdfSignTs -type ES_A -in odersky-signed-ES_A-ES_T.pdf      -out odersky-signed-ES_A-ES_T-ES_A.pdf
@rem PdfSignTs -type DTS  -in odersky-signed-ES_A-ES_T-ES_A.pdf -out odersky-signed-ES_A-ES_T-ES_A-DTS.pdf

%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList/odersky-signed-ES_A.pdf               -outFile %OUT_DIR%/011a.txt ^
 > %OUT_DIR%/011a.out 2>&1 ^
 && echo expected > %EC%/expected.011a || echo failure > %EC%/failure.011a

%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList/odersky-signed-ES_A-ES_T.pdf          -outFile %OUT_DIR%/011b.txt ^
 > %OUT_DIR%/011b.out 2>&1 ^
 && echo expected > %EC%/expected.011b || echo failure > %EC%/failure.011b

%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList/odersky-signed-ES_A-ES_T-ES_A.pdf     -outFile %OUT_DIR%/011c.txt ^
 > %OUT_DIR%/011c.out 2>&1 ^
 && echo expected > %EC%/expected.011c || echo failure > %EC%/failure.011c

%PdfTsExpirationList% -in %RESOURCE_DIR%/PdfTsExpirationList/odersky-signed-ES_A-ES_T-ES_A-DTS.pdf -outFile %OUT_DIR%/011d.txt ^
 > %OUT_DIR%/011d.out 2>&1 ^
 && echo expected > %EC%/expected.011d || echo failure > %EC%/failure.011d

exit /b 0
