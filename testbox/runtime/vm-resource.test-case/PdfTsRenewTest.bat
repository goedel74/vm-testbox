@echo off
setlocal enabledelayedexpansion
cd /d %~dp0

set PdfTsRenew=java jp.co.seiko_p.pdfts.cli.PdfTsRenew
set CONF_DIR=conf
set RESOURCE_DIR=test-resource
set OUT_DIR=%1/PdfTsRenew
mkdir %1\PdfTsRenew
set EC=exitCode/PdfTsRenew
mkdir exitCode\PdfTsRenew

@rem
@rem 環境変数 CLASSPATH は呼び出し元で設定されています。
@rem

echo ++++++++++++++++++++ PdfTsRenew Test: 001_002 引数なし ++++++++++++++++++++
mkdir watch-out
mkdir watch-renewed
copy %RESOURCE_DIR%\PdfTsRenew\expiration-list.txt                   watch-out.txt
copy %RESOURCE_DIR%\PdfTsRenew\odersky-signed.pdf                    watch-out\odersky-signed.pdf
copy %RESOURCE_DIR%\PdfTsRenew\odersky-signed-ES_A-ES_T-ES_A-DTS.pdf watch-out\odersky-signed-ES_A-ES_T-ES_A-DTS.pdf

%PdfTsRenew% ^
 > %OUT_DIR%/001_002.out 2>&1 ^
 && echo expected > %EC%/expected.001_002 || echo failure > %EC%/failure.001_002

mkdir %1\PdfTsRenew\001_002
xcopy watch-renewed %1\PdfTsRenew\001_002
del watch-out.txt
rmdir /S /Q watch-renewed
rmdir /S /Q watch-out


echo ++++++++++++++++++++ PdfTsRenew Test: 003_004_005_006_007 引数指定 / windowNumDays 14600 日 = 約 40 年 ++++++++++++++++++++
mkdir %1\PdfTsRenew\003_004_005_006_007

%PdfTsRenew% -outDir %OUT_DIR%/003_004_005_006_007 -inDir %RESOURCE_DIR%/PdfTsRenew ^
 -expirationListFile %RESOURCE_DIR%/PdfTsRenew/expiration-list.txt ^
 -windowNumDays 14600 -conf %CONF_DIR%/cli-config-copy.xml ^
 > %OUT_DIR%/003_004_005_006_007.out 2>&1 ^
 && echo expected > %EC%/expected.003_004_005_006_007 || echo failure > %EC%/failure.003_004_005_006_007

echo ++++++++++++++++++++ PdfTsRenew Test: 008 引数指定 errorCommand ++++++++++++++++++++
@rem 未署名の pdf を与えることで failed to sign/timestamp のエラーを意図的に起こす
mkdir watch-out

%PdfTsRenew% -outDir %OUT_DIR% ^
 -expirationListFile %RESOURCE_DIR%/PdfTsRenew/expiration-list-for-unsigned.txt ^
 -errorCommand "cmd /c copy nul %OUT_DIR%/008-error.txt" ^
 && echo failure > %EC%/failure.008 || echo expected > %EC%/expected.008

rmdir /Q watch-out

echo ++++++++++++++++++++ PdfTsRenew Test: 009_010_011_012 引数指定 proxy* ++++++++++++++++++++
mkdir watch-out
mkdir watch-renewed
copy %RESOURCE_DIR%\PdfTsRenew\expiration-list.txt                   watch-out.txt
copy %RESOURCE_DIR%\PdfTsRenew\odersky-signed.pdf                    watch-out\odersky-signed.pdf
copy %RESOURCE_DIR%\PdfTsRenew\odersky-signed-ES_A-ES_T-ES_A-DTS.pdf watch-out\odersky-signed-ES_A-ES_T-ES_A-DTS.pdf

%PdfTsRenew% -proxyHost 172.24.64.95 -proxyPort 13128 -proxyUser sysart-squid -proxyPassword 9eEn249Le ^
 > %OUT_DIR%/009_010_011_012.out 2>&1 ^
 && echo expected > %EC%/expected.009_010_011_012 || echo failure > %EC%/failure.009_010_011_012

mkdir %1\PdfTsRenew\009_010_011_012
xcopy watch-renewed %1\PdfTsRenew\009_010_011_012
del watch-out.txt
rmdir /S /Q watch-renewed
rmdir /S /Q watch-out

echo ++++++++++++++++++++ PdfTsRenew Test: 013 引数指定 tempDir / windowNumDays 14600 日 = 約 40 年 ++++++++++++++++++++
mkdir %1\PdfTsRenew\013
mkdir %1\PdfTsRenew\tmp-013

%PdfTsRenew% -outDir %OUT_DIR%/013 -inDir %RESOURCE_DIR%/PdfTsRenew ^
 -tempDir %OUT_DIR%/tmp-013 ^
 -expirationListFile %RESOURCE_DIR%/PdfTsRenew/expiration-list.txt ^
 -windowNumDays 14600 ^
 > %OUT_DIR%/013.out 2>&1 ^
 && echo expected > %EC%/expected.013 || echo failure > %EC%/failure.013

echo ++++++++++++++++++++ PdfTsRenew Test: 014 環境変数 tempDir / windowNumDays 14600 日 = 約 40 年 ++++++++++++++++++++
mkdir %1\PdfTsRenew\014
mkdir %1\PdfTsRenew\tmp-014

java -Djava.io.tmpdir=%OUT_DIR%/tmp-014 jp.co.seiko_p.pdfts.cli.PdfTsRenew -outDir %OUT_DIR%/014 -inDir %RESOURCE_DIR%/PdfTsRenew ^
 -expirationListFile %RESOURCE_DIR%/PdfTsRenew/expiration-list.txt ^
 -windowNumDays 14600 ^
 > %OUT_DIR%/014.out 2>&1 ^
 && echo expected > %EC%/expected.014 || echo failure > %EC%/failure.014

echo ++++++++++++++++++++ PdfTsRenew Test: 015 ++++++++++++++++++++
echo -useNopSslHostnameVerifier 指定は実施しない on 2018-06 > %EC%/skip.015

echo ++++++++++++++++++++ PdfTsRenew Test: 016 help ++++++++++++++++++++
%PdfTsRenew% -help ^
 > %OUT_DIR%/016.out 2>&1 ^
 && echo failure > %EC%/failure.016 || echo expected > %EC%/expected.016

exit /b 0
