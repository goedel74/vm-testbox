#!/bin/bash

set -xeu

# parameters
readonly DONE_FILE="task.`hostname`.done"
readonly LOG_FILE="task.`hostname`.boxplayer-test.last.log"
readonly TEST_EVIDENCE_DIR="task.`hostname`.last.out"
readonly UNIT_LOG_FILE="`hostname`.java-test.service.last.log"
jre_ver= # set later
java_cp= # set later

# trap
trap finally EXIT
function ssh_upload() {
	# upload: VirtualBox specs host-ip = 10.0.2.2
	if test -e "$1"; then
		scp "$1" vm@10.0.2.2:result
	fi
	return 0
}
function finally() {
	RV=$?
	if type journalctl > /dev/null 2>&1; then
		journalctl -u java-test > $UNIT_LOG_FILE; ssh_upload "$UNIT_LOG_FILE"
	fi
	ssh_upload "$LOG_FILE"
	touch $DONE_FILE; echo "{ \"exitCode\": $RV }" > $DONE_FILE; ssh_upload "$DONE_FILE"
	return 0
}

# clean
rm -rf last; mkdir last; cd last

# download
scp vm@10.0.2.2:vm-resource/work/jre_version .
scp vm@10.0.2.2:vm-resource/work/lib.zip .
scp vm@10.0.2.2:vm-resource/body.sh .

chmod +x body.sh
unzip lib.zip # which generates lib/ directory
mkdir $TEST_EVIDENCE_DIR

# execute
jre_ver=`cat jre_version`
export JAVA_HOME=$jre_ver
export PATH=$JAVA_HOME/bin:$PATH

java_cp="."
for jar in lib/*.jar;
do
	java_cp="$java_cp:$jar"
done

./body.sh $TEST_EVIDENCE_DIR $java_cp > $LOG_FILE 2>&1

# post process
mv body.sh $TEST_EVIDENCE_DIR
mv jre_version $TEST_EVIDENCE_DIR
zip -r $TEST_EVIDENCE_DIR.zip $TEST_EVIDENCE_DIR
ssh_upload "$TEST_EVIDENCE_DIR.zip"
