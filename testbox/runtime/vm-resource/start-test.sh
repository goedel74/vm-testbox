#!/bin/bash

set -xeu

# parameters
EXE_FILE="exec-test.sh"

# clean
rm -f $EXE_FILE

# download
scp vm@10.0.2.2:vm-resource/$EXE_FILE .
chmod +x $EXE_FILE

# execute
sh $EXE_FILE
