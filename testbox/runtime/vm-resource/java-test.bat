@echo off

rem
rem porting java-test.service
rem

cd C:\Programs\java-test\work

rem
rem porting start-test.sh
rem

@echo on

set SSH_HOME=C:\Programs\OpenSSH-Win64
set PATH=%SSH_HOME%;%PATH%

set EXE_FILE=exec-test.bat
del %EXE_FILE%
scp -i ../id_rsa vm@10.0.2.2:vm-resource/%EXE_FILE% .
start %EXE_FILE%
