@echo on
setlocal enabledelayedexpansion

@rem improve: try/catch/finally like (bash) trap & set -e
set FIN=false

cd /d %~dp0

@rem parameters
for /F %%I IN ('hostname') DO (
	set HOSTNAME=%%I
)
set DONE_FILE=task.%HOSTNAME%.done
set TEST_EVIDENCE_DIR=task.%HOSTNAME%.last.out
set SSH_PK=..\..\id_rsa

set Z7_HOME=C:\Program Files\7-Zip
set PATH=%Z7_HOME%;%PATH%

(

rmdir /S /Q last
timeout 1
mkdir last
timeout 1
cd last
timeout 1

) && (

scp -i %SSH_PK% vm@10.0.2.2:vm-resource.work/test-case.zip . && ^
7z x -y test-case.zip
copy /Y test-case\* .

scp -i %SSH_PK% vm@10.0.2.2:vm-resource.work/test-resource.zip . && ^
7z x -y test-resource.zip

scp -i %SSH_PK% vm@10.0.2.2:vm-resource.work/jre_version .

mkdir exitCode
mkdir %TEST_EVIDENCE_DIR%
mkdir conf

) && (

copy test-resource\conf\. conf

rem set JAVA_HOME=
rem for /F "delims=," %%I IN ('type jre_version') DO (
rem 	set JAVA_HOME=%%I
rem )
rem set PATH=%JAVA_HOME%\bin;%PATH%

set CLASSPATH=.;conf
for /F %%I in ('dir /B test-resource\*.jar') do (
	set CLASSPATH=!CLASSPATH!;test-resource\%%I
)

@rem @echo off
call entry.bat %TEST_EVIDENCE_DIR%
@echo on

) && (

copy test-case\* %TEST_EVIDENCE_DIR% && rmdir /S /Q test-case
for /F %%I in ('dir /B log\*.log') do (
	copy log\%%I %TEST_EVIDENCE_DIR%
)

7z a exitCode.zip exitCode
scp -i %SSH_PK% exitCode.zip vm@10.0.2.2:result

7z a %TEST_EVIDENCE_DIR%.zip %TEST_EVIDENCE_DIR%
scp -i %SSH_PK% %TEST_EVIDENCE_DIR%.zip vm@10.0.2.2:result

) && (

set FIN=true

)

set RV=1
if "%FIN%"=="true" (
	set RV=0
)
echo { "exitCode": %RV% } > %DONE_FILE%

scp -i %SSH_PK% %DONE_FILE% vm@10.0.2.2:result

exit /b %ERRORLEVEL%
