import java.io.IOException
import java.net.{DatagramSocket, ServerSocket, InetSocketAddress}
// import javax.net.ssl.SSLSocket
// import scala.util.control.NonFatal


object WaitForPortOpen extends App {
	lazy val intervalInSec = 4 // 固定

	new WaitForPortOpen(
		portNumber = args(0).toInt
	).run
}

class WaitForPortOpen(portNumber: Int,
		host: String = "127.0.0.1",
		intervalInSec: Int = WaitForPortOpen.intervalInSec) {

	// http://svn.apache.org/viewvc/camel/trunk/components/camel-test/src/main/java/org/apache/camel/test/AvailablePortFinder.java?view=markup#l130
	def portAvailable = {
		println(s"scan port = $portNumber")

		var notOpen: Boolean = false
		var ss: ServerSocket = null
		try {
			ss = new ServerSocket()
			ss.setReuseAddress(true)
			ss.bind(new InetSocketAddress(host, portNumber))
		} catch {
			case ex: IOException =>
				println(s"ignore exception on ss.bind: ${ex.getClass} / ${Option(ex.getMessage)}")
				notOpen = true
		} finally {
			if (ss != null) try ss.close catch {
				case ex: IOException =>
					println(s"ignore exception on ss.close: ${ex.getClass} / ${Option(ex.getMessage)}")
			}
		}

		if (notOpen) {
			notOpen = false
			var ds: DatagramSocket = null
			try {
				ds = new DatagramSocket()
				ds.setReuseAddress(true)
				ds.bind(new InetSocketAddress(host, portNumber))
			} catch {
				case ex: IOException =>
					println(s"ignore exception on ds.bind: ${ex.getClass} / ${Option(ex.getMessage)}")
					notOpen = true
			} finally {
				if (ds != null) try ds.close catch {
					case ex: IOException =>
						println(s"ignore exception on ds.close: ${ex.getClass} / ${Option(ex.getMessage)}")
				}
			}
		}

		notOpen
	}

	// タイムアウト値は呼び出し元(外部)で制御してもらう想定
	def run(): Unit = {
		println(s"waiting for port open in every $intervalInSec sec: $portNumber")
		var open = false
		while (!open) {
			open = portAvailable
			if (!open) Thread.sleep(intervalInSec * 1000)
		}
	}

}
