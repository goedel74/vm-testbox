# Jenkins


***


* `java -jar $JENKINS_HOME/jenkins.war --httpPort=8103 --httpsPort=8104 > $JENKINS_HOME/jenkins.log 2>&1`
* access `http://localhost:8103`


***


* plugin
	* [Job Configuration History](https://plugins.jenkins.io/jobConfigHistory)
	* [Slack Plugin](https://wiki.jenkins.io/display/JENKINS/Slack+Plugin)
	* [Pipeline Utility Steps](https://plugins.jenkins.io/pipeline-utility-steps)
		* readJSON
* job setting
	* disable 'Use Groovy Sandbox' to avoid 'Scripts not permitted'


***


![](Jekins-minimal-settings.png)

