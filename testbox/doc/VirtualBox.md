# VirtualBox


***


## CentOS guest preparation

* prepare
	* install guest os on VirtualBox
		* (CentOS common)
			* vm name = CentOS-${major_version}-testbox-template
			* ホスト名 = CentOS-${major_version}-testbox-on-time
			* root password = `secret`
			* create user `boxplayer` as administrator / password not reuired
			* VirtualBox Guest Additions to improve mouse and display
			* `sudo vi /etc/grub2.cfg` (or `/etc/grub.conf`) → modify `set timeout=5` to `set timeout=1`
		* CentOS 7.5 (latest) ～ 64 bit
			* ソフトウェアの選択 = サーバー (GUI 使用)
				+ ネットワークファイルシステムクライアント
				+ パフォーマンスツール
				+ 開発ツール
				+ セキュリティツール
				+ システム管理ツール
			* Ethernet = オン
			* accept 位置情報の取得
			* GUI > アプリケーション > システムツール > 設定 > 詳細 > ユーザー > boxplayer > 自動ログイン = オン
		* CentOS 6.9 ～ 32 bit
			* http://ftp.riken.jp/Linux/centos/6.9/os/i386/ for netinstall
			* software = Desktop
				+ Base System > Networking Tools
				+ Base System > Performance Tools
				+ Base System > Hardware monitoring utilities
				+ Base System > Security Tools
				+ Development > Development Tools
				+ Servers > System administration tools
			* disable Kdump
			* user
				* `root# vi /etc/gdm/custom.conf`
					```
					[daemon]
					TimedLoginEnable=true
					TimedLogin=boxplayer
					TimedLoginDelay=1
					```
				* `root# usermod -aG wheel boxplayer`
				* `root# visudo`
					* add line `boxplayer ALL=(ALL) ALL` below `root ALL=(ALL) ALL`
					* add line `boxplayer ALL=(ALL) NOPASSWD: ALL` below `# %wheel ALL=(ALL) NOPASSWD: ALL`
			* GUI > システム > 設定 > 電源管理 > ディスプレイ > 画面をスリープ = しない
		* CentOS 5.11 ～ 32 bit
			* http://vault.centos.org/5.11/os/i386/ for netinstall
			* ソフトウェア = サーバ GUI
				+ アプリケーション > グラフィカルインターネット
				+ ベースシステム > システムツール
				+ 開発 > 開発ツール
			* SELinux 設定 = Permissive
			* user
				* `root# /usr/sbin/usermod -aG wheel boxplayer`
				* `root# /usr/sbin/visudo`
					* (CentOS 6 と同じ)
			* hostname
				* `root# vi /etc/hosts`
				* `root# vi /etc/sysconfig/network`
			* GUI > システム > 管理 > ログイン画面 > ユーザ > 対象となるユーザ > 追加 > boxplayer
			* GUI > システム > 管理 > ログイン画面 > セキュリティ > 自動ログインを有効にする > boxplayer
			* GUI > システム > 設定 > スクリーンセーバー > スクリーンセーバーを起動 = (チェックを外す)
	* host$ `sbt "testbox/runMain PrivateSshServer foo.txt 60 22"`
		* to get files below from host machine
	* ssh
		* `mkdir /home/boxplayer/.ssh/`
		* `chmod 700 /home/boxplayer/.ssh/`
		* 閉鎖的な環境で自動で動作させることを目的に秘密鍵をホスト側から取得
			* `scp vm@10.0.2.2:ssh-client-resource/id_rsa /home/boxplayer/.ssh/`
			* accept known_hosts
			* empty password
		* `chmod 600 /home/boxplayer/.ssh/id_rsa`
	* `mkdir /home/boxplayer/work/`

* install java `TODO` Oracle-JRE
	* CentOS 7
		```
		sudo yum install -y java-1.6.0-openjdk.x86_64
		sudo yum install -y java-1.7.0-openjdk.x86_64
		sudo yum install -y java-1.8.0-openjdk.x86_64
		echo "/usr/lib/jvm/java-1.6.0-openjdk-1.6.0.41.x86_64/jre"                 > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.6
		echo "/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.181-2.6.14.8.el7_5.x86_64/jre" > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.7
		echo "/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.171-8.b10.el7_5.x86_64/jre"    > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.8
		```
	* CentOS 6
		```
		sudo yum install -y java-1.6.0-openjdk-devel.i686
		sudo yum install -y java-1.7.0-openjdk-devel.i686
		sudo yum install -y java-1.8.0-openjdk-devel.i686
		echo "/usr/lib/jvm/jre-1.6.0-openjdk"      > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.6
		echo "/usr/lib/jvm/jre-1.7.0-openjdk"      > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.7
		echo "/usr/lib/jvm/jre-1.8.0-openjdk.i386" > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.8
		```
	* CentOS 5
		* `sudo mkdir /usr/java; cd /usr/java/`
		* jre6 : get an rpm.bin file from Oracle
			```
			sudo chmod +x jre-6u45-linux-i586-rpm.bin
			sudo ./jre-6u45-linux-i586-rpm.bin
			echo "/usr/java/jre1.6.0_45" > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.6
			```
		* jre7 : get an rpm file from Oracle
			```
			sudo rpm -ivh --replacefiles ./jre-7u80-linux-i586.rpm
			echo "/usr/java/jre1.7.0_80" > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.7
			```
		* jre8 : get an rpm file from Oracle
			```
			sudo rpm -ivh ./jre-8u162-linux-i586.rpm
			echo "/usr/java/jre1.8.0_162" > jre_version; scp jre_version vm@10.0.2.2:vm-resource/`hostname`.jre_1.8
			```
	* `rm -f jre_version`

* systemd/initd
	* `scp vm@10.0.2.2:vm-resource/start-test.sh /home/boxplayer/work/`
	* `chmod +x /home/boxplayer/work/start-test.sh`
	* CnetOS 7
		* `sudo scp vm@10.0.2.2:vm-resource/java-test.service /etc/systemd/system/`
		* `sudo systemctl enable java-test`
	* CentOS 5, 6
		* `sudo scp vm@10.0.2.2:vm-resource/java-test /etc/init.d/`
		* `sudo chmod +x /etc/init.d/java-test`
		* `sudo /sbin/chkconfig --add java-test` or `java-test-CentOS5`
		* `sudo /sbin/chkconfig --level 3 java-test on`

* vm-export
	* host$ `VboxManage export CentOS-7-testbox-template -o CentOS-7-testbox-on-time.ova`
	* host$ `VboxManage export CentOS-6-testbox-template -o CentOS-6-testbox-on-time.ova`
	* host$ `VboxManage export CentOS-5-testbox-template -o CentOS-5-testbox-on-time.ova`

* vm-import (improve: --vsys 0)
	* host$ `VboxManage import CentOS-7-testbox-on-time.ova --vsys 0 --vmname CentOS-7-testbox-on-time`
	* host$ `VboxManage import CentOS-6-testbox-on-time.ova --vsys 0 --vmname CentOS-6-testbox-on-time`
	* host$ `VboxManage import CentOS-5-testbox-on-time.ova --vsys 0 --vmname CentOS-5-testbox-on-time`


***


## Windows Server 2016 guest preparation

* 共通
	* 前提
		* OS がインストールされた vm が存在する
		* その vm で zip/unzip が出来る (e.g. 7z)
		* その vm に jre がインストールされている
	* ホスト名変更
		* WinS-2016-j8
		* WinS-2016-j7
		* WinS-2016-j6
	* Windows update 無効化 と シャットダウン理由無効化
		* Windows > Windows システムツール > ファイル名を指定して実行 > `gpedit.msc`
		* コンピュータの構成 > 管理用テンプレート
			* Windows コンポーネント > Windows Update > 自動更新を構成する > 無効 > 適用
			* システム > シャットダウンイベントの追跡ツールを表示する > 無効 > 適用
	* ディレクトリ準備
		* C:\Programs\wrok
	* 自動ログイン有効化
		* コマンドプロンプト > `netplwiz` > 「パスワードの入力が必要」のチェックを外す
	* 環境変数
		* JAVA_HOME 設定
		* %JAVA_HOME%\bin に PATH を通す
	* ssh
		* [Win32-OpenSSH](https://github.com/PowerShell/Win32-OpenSSH/releases)
		* 最新版を C:\Programs にダウンロード
		* unzip OpenSSH-Win64.zip
		* 展開ディレクトリに PATH を通す
		* host で `sbt "testbox/runMain PrivateSshWaitFile foo.txt 22"`
			* `improve` 本来は 19022 ポートで起動したいがクライアント側で指定できない → Win32-OpenSSH にバグがあるのかもしれない
		* `scp vm@10.0.2.2:ssh-client-resource/id_rsa C:\Programs`
			* ファイルのプロパティ > セキュリティ > 詳細設定 : 継承の無効化 > 継承されたアクセス許可をこのオブジェクトの明示的なアクセス許可に変換します。
			* ファイルのプロパティ > セキュリティ : SYSTEM, Administrators 以外のアクセス許可リストを削除
	* 起動時処理ファイルの取得
		* `scp vm@10.0.2.2:vm-resource\java-test.bat C:\Programs\work`
	* スタートアップへの登録
		* bat ファイルのショートカット作成
		* Windows > Windows システムツール > ファイル名を指定して実行 > `shell:startup`
		* C:\Users\Administrator\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
		* 上記が開くのでショートカットファイルを D&D
	* その他
		* フォルダ表示オプションの調整
	* 再起動してから各種の作業結果を確認

* jre8
	* TODO

* jre7
	* TODO

* jre6
	* (個別の作業無し)
