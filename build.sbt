lazy val commonSettings = Seq(
	scalaVersion := "2.12.6",
	scalacOptions ++= Seq(
		"-Xlint", "-Xfuture", "-feature", "-deprecation", "-unchecked"
	)
)

lazy val testbox = (project in file("testbox")).
	settings(commonSettings: _*).
	settings(
		name := "vm-testbox",
		version := "0.1",
		libraryDependencies ++= Seq(
			"org.apache.sshd" % "apache-sshd" % "1.7.0"
		),
		baseDirectory in run := file("."),
		publish := {},
		publishLocal := {},
		fork := true
	)

cancelable in Global := true // enable Ctrl-C
