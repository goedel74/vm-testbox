# コマンド

* sbt> `project testbox`
* sbt> `compile`
* sbt> `runMain PrivateSshWaitFile foo.txt 19022`
* sbt> `runMain WaitForPortOpen 19022`
* sbt> `clean`

# ファイル・ディレクトリ

* testbox
	* テスト環境 (git/sbt layout)
* prodbox
	* テスト対象のソフトウェア (git/sbt layout)
	* ~~テストケースを含んでいる想定~~
* project
	* ビルド関連資材
* build.sbt
	* ビルドスクリプト
* README.md
	* このファイル
